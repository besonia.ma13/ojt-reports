.TL
End of the Day Report
.AU
Mark Nhel A. Besonia
.AI
Innovuze Solutions, Inc
.DA

.QP
.IP \(bu 2
Assisted admin task.
.IP \(bu 2
Installed and configured
.I "minikube"
and created sample deployments and services.
